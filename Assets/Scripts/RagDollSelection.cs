﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagDollSelection : MonoBehaviour
{
    #region SERIALIZED FIELDS
    [SerializeField]
    private int ragDollID;
    [SerializeField]
    private Renderer renderer;
    #endregion

    #region PRIVATE FIELDS
    private bool selected;
    #endregion

    #region PROPERTIES
    public bool Selected
    {
        get => selected;
        set => selected = value;
    }
    #endregion

    #region MONO BEHAVIOURS
    private void OnMouseEnter()
    {
        if (Room1Control.Instance.SelectionTime && !selected)
        {
            renderer.material.color = Color.red;
        }
    }

    private void OnMouseExit()
    {
        if (Room1Control.Instance.SelectionTime && !selected)
        {
            renderer.material.color = Room1Control.Instance.RagDollNormalColor;
        }
    }
    private void OnMouseDown()
    {
        if (Room1Control.Instance.SelectionTime)
        {
            if (!selected)
            {
                renderer.material.color = Color.blue;
                Room1Control.Instance.RagdollSelected(ragDollID);
            }
            else
            {
                renderer.material.color = Room1Control.Instance.RagDollNormalColor;
                Room1Control.Instance.RagdollDeselected(ragDollID);
            }
           
        }
    }
    private void OnMouseUp()
    {
        if (Room1Control.Instance.SelectionTime)
        {
            selected = !selected;
        }
    }
    #endregion
}

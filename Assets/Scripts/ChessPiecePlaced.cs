﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessPiecePlaced : MonoBehaviour
{
    #region SERIALIZED FIELDS
    [SerializeField]
    private bool occupied;
    #endregion

    #region MONO BEHAVIOURS
    private void OnCollisionEnter(Collision collision)
    {
        if (!occupied && collision.gameObject.tag == "castle")
        {
            if (!collision.gameObject.GetComponent<Castle>().IsUsed)
            {
                collision.gameObject.GetComponent<Castle>().IsUsed = true;
                occupied = true;
                Room1Control.Instance.ChessPiecePlaced();
            }
        }
    }
    #endregion
}

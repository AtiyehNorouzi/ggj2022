﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleArea : MonoBehaviour
{
    #region MONO BEHAVIOURS
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Torso" && Room1Control.Instance.TimeRewind)
        {
            other.gameObject.transform.parent.transform.parent.transform.parent.gameObject.SetActive(false);
        }
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Room1Control : MonoBehaviour
{
    #region SERIALIZED FIELDS
    [SerializeField]
    private Renderer lampMat;
    [SerializeField]
    private List<Ragdoll> ragdolls;
    [SerializeField]
    private List<Renderer> ragdollMats;
    [SerializeField]
    private GameObject leader;
    [SerializeField]
    private Transform nearTheDoor;
    [SerializeField]
    private GameObject cameraObj;
    [SerializeField]
    private GameObject canvas;
    [SerializeField]
    private GameObject cryptoPanel;
    [SerializeField]
    private GameObject gameOverPanel;
    #endregion

    #region CONST FIELDS
    private int desiredChessPlaced = 4;
    private int selectionLimit = 3;
    private Collider rhymeCollider;
    #endregion

    #region STATIC FIELDS
    private static Room1Control _instance;
    #endregion

    #region PRIVATE FIELDS
    private HashSet<int> indexSet;
    private List<int> rhymeCode = new List<int> { 0, 2, 4 };
    private int currentChessplaced;
    private bool inArea;
    private Color ragDollNormalColor;
    private bool selectionTime;
    private bool timeRewind;
    private bool hangTime;
    #endregion

    #region PROPERTIES
    public static Room1Control Instance => (_instance == null) ? _instance = FindObjectOfType<Room1Control>() : _instance;

    public GameObject Leader
    {
        get => leader;
        set => leader = value;
    }
    public Color RagDollNormalColor
    {
        get => ragDollNormalColor;
        set => ragDollNormalColor = value;
    }
    public bool SelectionTime
    {
        get => selectionTime;
        set => selectionTime = value;
    }
    public bool TimeRewind
    {
        get => timeRewind;
        set => timeRewind = value;
    }
    public bool HangTime
    {
        get => hangTime;
        set => hangTime = value;
    }
    #endregion

    #region MONO BEHAVIOURS
    private void Start()
    {
        indexSet = new HashSet<int>();
        RagDollNormalColor = ragdollMats[0].sharedMaterial.color;
        rhymeCollider = GetComponent<Collider>();
    }

    void Update()
    {
        if (currentChessplaced >= desiredChessPlaced)
        {
            lampMat.material.SetColor("_EmissionColor", Color.green);
            if (Vector2.Distance(leader.transform.position, nearTheDoor.position) < 3 && !inArea)
            {
                SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Rhyme);
                inArea = true;
                StartCoroutine(nameof(RhymeCoroutine));
            }
            if (SelectionTime && indexSet.Count >= selectionLimit)
            {
                SelectionTime = false;
                if (correctSelection())
                {
                    StartCoroutine(nameof(HangRoutine));
                    HangTime = true;
                }
                else
                {
                    cryptoPanel.SetActive(false);
                    canvas.SetActive(true);
                    gameOverPanel.SetActive(true);
                }
            }
            if (TimeRewind)
            {
                if (TimeBody.pointLeft <= 0)
                {
                    SceneManager.LoadScene("KillScene");
                }
            }
        }
    }
    #endregion

    #region PUBLIC METHODS
    public void ChessPiecePlaced()
    {
        ragdolls[currentChessplaced].ActivateFollower();
        currentChessplaced += 1;
    }

    public void ChessPieceUnPlaced()
    {
        currentChessplaced -= 1;
    }

    public void RagdollSelected(int index)
    {
        indexSet.Add(index);
    }

    public void RagdollDeselected(int index)
    {
        indexSet.Remove(index);
    }
    public void Retry()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }
    #endregion

    #region PRIVATE METHODS
    private bool correctSelection()
    {
        for (int i = 0; i < rhymeCode.Count; i++)
        {
            if (!indexSet.Contains(rhymeCode[i]))
                return false;
        }
        return true;
    }
    #endregion

    #region IENUMERATORS
    IEnumerator HangRoutine()
    {
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Hang);
        cameraObj.SetActive(true);
        yield return new WaitForSeconds(2);
        leader.GetComponent<Ragdoll>().HangedInitiate();
        yield return new WaitForSeconds(2);
        ragdolls[1].HangedInitiate();
        yield return new WaitForSeconds(2);
        ragdolls[3].HangedInitiate();
        yield return new WaitForSeconds(2);
        canvas.SetActive(true);
        cryptoPanel.SetActive(true);
        gameOverPanel.SetActive(false);
        yield return new WaitForSeconds(3);
        cryptoPanel.SetActive(false);
        canvas.SetActive(false);
        TimeRewind = true;
    }
    IEnumerator RhymeCoroutine()
    {
        yield return new WaitForSeconds(5);
        ragdollMats[0].material.color = Color.yellow;
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Dummy);
        yield return new WaitForSeconds(2);
        ragdollMats[0].material.color = RagDollNormalColor;
        ragdollMats[1].material.color = Color.yellow;
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Bunny);
        yield return new WaitForSeconds(2);
        ragdollMats[1].material.color = RagDollNormalColor;
        ragdollMats[2].material.color = Color.yellow;
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Mommy);
        yield return new WaitForSeconds(2);
        ragdollMats[2].material.color = RagDollNormalColor;
        ragdollMats[3].material.color = Color.yellow;
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Monkey);
        yield return new WaitForSeconds(2);
        ragdollMats[3].material.color = RagDollNormalColor;
        ragdollMats[4].material.color = Color.yellow;
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Tommy);
        yield return new WaitForSeconds(2);
        ragdollMats[4].material.color = RagDollNormalColor;
        SelectionTime = true;
    }
    #endregion

}

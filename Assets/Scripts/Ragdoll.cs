﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActiveRagdoll;
public class Ragdoll : MonoBehaviour
{
    #region SERIALIZED FIELDS
    [SerializeField]
    private bool isLeader;

    [SerializeField]
    private GameObject hangedGameObject;
    #endregion

    #region MONO BEHAVOIURS
    void Update()
    {
        if (!isLeader)
        {
            if (Room1Control.Instance.HangTime)
            {
                GetComponent<CameraModule>().enabled = false;
            }
        }
    }
    #endregion

    #region PUBLIC METHODS
    public void HangedInitiate()
    {
        hangedGameObject.transform.position = new Vector3(transform.position.x, 2.5f , transform.position.z);
        hangedGameObject.transform.rotation = transform.rotation;
        hangedGameObject.SetActive(true);
        gameObject.SetActive(false);
    }
    public void ActivateFollower()
    {
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Fall);
        gameObject.SetActive(true);
    }
    #endregion
}

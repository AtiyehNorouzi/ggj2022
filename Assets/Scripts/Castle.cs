﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Castle : MonoBehaviour
{
    #region PRIVATE FIELDS
    private bool isUsed;
    #endregion
    #region PROPERTIES
    public bool IsUsed
    {
        get => isUsed;
        set => isUsed = value;
    }
    #endregion

}
